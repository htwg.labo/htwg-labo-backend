'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">htwg-labo-backend documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' : 'data-target="#xs-controllers-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' :
                                            'id="xs-controllers-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' : 'data-target="#xs-injectables-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' :
                                        'id="xs-injectables-links-module-AppModule-d816734bf17c4a59f2285a0a35cb4dfa45f0864563873418569f02673770e204b134e86ed9506163d82c1d77f932d4cc55f1cde7743a6a03b51d3b80e21bd2ba"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link" >AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' : 'data-target="#xs-controllers-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' :
                                            'id="xs-controllers-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' : 'data-target="#xs-injectables-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' :
                                        'id="xs-injectables-links-module-AuthModule-5d00d69d6ee970359678e44e9076cb7783130b8ca4498f87b4d2db3e3389a87bf65b683a5276e9cf344e9170298027671524e30e1885adac67dac43ef9507ab9"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JwtStrategy.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtStrategy</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LocalStrategy.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LocalStrategy</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/UserModel.html" data-type="entity-link" >UserModel</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});