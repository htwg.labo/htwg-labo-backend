import { Controller, Get, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { UserModel } from 'src/models/user.model';
import { UserManagementService } from '../service/user-management.service';

@Controller('user-management')
export class UserManagementController {

    constructor(private _userManagementService: UserManagementService){}

    /**
     * Gets the user data from moodle.
    */
     @Get('/getUser')
     @UsePipes(new ValidationPipe())
     async userId(@Query('token') token: string): Promise<UserModel> {
         return this._userManagementService.getUserDataFromMoodle(token);
     }

    /**
     * Gets a list of all participants/students of a moodle course.
    */
     @Get('/getCourseParticipants')
     @UsePipes(new ValidationPipe())
     async courseParticipants(@Query() query: {token: string, courseId: number}): Promise<UserModel[]> {
         return this._userManagementService.getCourseParticipantsFromMoodle(query.token, query.courseId);
     }
}
