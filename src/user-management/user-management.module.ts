import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UserManagementController } from './controller/user-management.controller';
import { UserManagementService } from './service/user-management.service';

@Module({
  imports: [
    HttpModule,
    ConfigModule,
  ],
  controllers: [UserManagementController],
  providers: [UserManagementService],
  exports: [UserManagementService]
})
export class UserManagementModule {}
