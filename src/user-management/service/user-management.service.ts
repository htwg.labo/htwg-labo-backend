import { HttpService } from '@nestjs/axios';
import { HttpException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom, map, tap } from 'rxjs';
import { TokenModel } from 'src/models/token.model';
import { UserModel } from 'src/models/user.model';

@Injectable()
export class UserManagementService {
    
    private readonly _moodleBaseUrl = this._configServic.get<string>('moodleBaseUrl');

    constructor(
        private readonly _httpService: HttpService, 
        private readonly _configServic: ConfigService,
    ){}

    /**
     * Gets the user data from moodle.
    */
     async getUserDataFromMoodle(token: string): Promise<UserModel> {
        let user: UserModel;
        const httpRequest = this._httpService.get(this._moodleBaseUrl + '/webservice/rest/server.php?wstoken=' + token + '&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json');
        const userResponse = await lastValueFrom(httpRequest.pipe(
            catchError(e => {
                throw new HttpException(e.response.data, e.response.status);
            }),
            map(resp => resp.data),
            tap(data => {
                if(
                    data.hasOwnProperty('username') &&
                    data.hasOwnProperty('firstname') &&
                    data.hasOwnProperty('lastname') &&
                    data.hasOwnProperty('userid') &&
                    data.hasOwnProperty('userpictureurl') 
                ){
                    user = {
                        username: data.username,
                        firstname: data.firstname,
                        lastname: data.lastname,
                        userid: data.userid,
                        userpictureurl: data.userpictureurl
                    };                    
                }
            }),
        ));
        if(user){
            return user;
        }
        return userResponse
    }

    /**
     * Gets a list of all participants/students of a moodle course.
    */
     async getCourseParticipantsFromMoodle(token: string, courseId: number): Promise<UserModel[]> {
        let participants: UserModel[] = [];
        const httpRequest = this._httpService.get(this._moodleBaseUrl + '/webservice/rest/server.php?wstoken=' + token + '&wsfunction=core_enrol_get_enrolled_users&courseid='+ courseId + '&moodlewsrestformat=json');
        const userResponse = await lastValueFrom(httpRequest.pipe(
            catchError(e => {
                throw new HttpException(e.response.data, e.response.status);
            }),
            map(resp => resp.data),
            tap(data => {
                if(Array.isArray(data)){
                    participants = [];
                    data.forEach(user => {
                        user.roles.forEach((role:any)=>{
                            if(role.shortname === 'student'){
                                participants.push({
                                    username: user.username,
                                    firstname: user.firstname,
                                    lastname: user.lastname,
                                    userid: user.id,
                                    userpictureurl: user.profileimageurl
                                });                                
                            }
                        })
                    })
                }
            })
        ));
        if(participants){
            return participants;
        }
        return userResponse
    }
}
