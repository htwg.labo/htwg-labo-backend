import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ExerciseManagementModule } from './exercise-management/exercise-management.module';
import { UserManagementModule } from './user-management/user-management.module';
import configuration from './config/configuration';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    ConfigModule.forRoot(
      {
        isGlobal: true,
        load: [configuration],
      }
    ),
    MongooseModule.forRoot('mongodb+srv://htwglabo:kbUDbwQvqvl7rwYl@cluster0.1v4rbzm.mongodb.net/?retryWrites=true&w=majority'),
    AuthModule,
    ExerciseManagementModule,
    UserManagementModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
