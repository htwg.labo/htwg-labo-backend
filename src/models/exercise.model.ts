import { IsBoolean, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class ExerciseModel {
    @IsNumber()
    @IsNotEmpty()
    courseid: number;

    @IsString()
    @IsNotEmpty()
    coursefullname: string;

    @IsString()
    @IsNotEmpty()
    courseshortname: string;

    @IsNumber()
    @IsNotEmpty()
    id: number;

    @IsString()
    @IsNotEmpty()
    name: string;

    @IsNumber()
    @IsNotEmpty()
    duedate: number;

    @IsNumber()
    @IsNotEmpty()
    allowsubmissionsfromdate: number;

    @IsString()
    @IsNotEmpty()
    intro: string;

    @IsBoolean()
    @IsNotEmpty()
    hasDefinedSteps: boolean;
}