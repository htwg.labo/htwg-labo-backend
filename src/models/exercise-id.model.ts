import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class ExerciseIdModel {
    @IsNumber()
    @IsNotEmpty()
    exerciseId: number;
}