import { IsBoolean, IsNotEmpty, IsNumber } from "class-validator";

export class DeletedModel {
    @IsBoolean()
    @IsNotEmpty()
    acknowledged: boolean;

    @IsNumber()
    @IsNotEmpty()
    deletedCount: number;
}