import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class TokenParticipantsModel {
    @IsString()
    @IsNotEmpty()
    token: string;

    @IsNumber()
    @IsNotEmpty()
    courseId: number;
}