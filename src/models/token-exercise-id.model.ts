import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class TokenExerciseIdModel {
    @IsString()
    @IsNotEmpty()
    token: string;

    @IsNumber()
    @IsNotEmpty()
    exerciseId: number;
}