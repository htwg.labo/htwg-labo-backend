import { IsNotEmpty, IsString } from "class-validator";

export class AuthUserModel {
    @IsString()
    @IsNotEmpty()
    username: string;

    @IsString()
    @IsNotEmpty()
    password: string;
}