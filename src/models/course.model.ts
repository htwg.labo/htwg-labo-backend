import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CourseModel {
    @IsNumber()
    @IsNotEmpty()
    id: number;

    @IsString()
    @IsNotEmpty()
    fullname: string;

    @IsString()
    @IsNotEmpty()
    shortname: string;
}