import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import * as mongoose from 'mongoose';

export const ExerciseWithStepsSchema = new mongoose.Schema({
    exerciseId: {type: Number, required: true},
    steps: {type: [
        {
            name: {type: String, required: true},
            description: {type: String, required: false},
            order: {type: Number, required: true},
        }
    ], required: true}
});

export class ExerciseWithStepsModel{
    @IsNumber()
    @IsNotEmpty()    
    exerciseId: number;

    @IsNotEmpty()
    steps: ExerciseStep[];
}

export class ExerciseStep{
    @IsString()
    @IsNotEmpty()    
    name: string;

    @IsString()
    @IsOptional()  
    description: string;

    @IsNumber()
    @IsNotEmpty()   
    order: number;
}