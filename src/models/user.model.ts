import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class UserModel {
    @IsString()
    @IsNotEmpty()
    username: string;

    @IsString()
    @IsNotEmpty()
    firstname: string;

    @IsString()
    @IsNotEmpty()
    lastname: string;

    @IsNumber()
    @IsNotEmpty()
    userid: number;

    @IsString()
    @IsNotEmpty()
    userpictureurl: string;
}