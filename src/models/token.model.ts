import { IsNotEmpty, IsString } from "class-validator";

export class TokenModel {
    @IsString()
    @IsNotEmpty()
    token: string;
}