import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class ExerciseStepIdModel {
    @IsString()
    @IsNotEmpty()
    exerciseStepId: string;

    @IsNumber()
    @IsNotEmpty()
    userId: number;

    @IsNumber()
    @IsNotEmpty()
    exerciseId: number;
}