import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import * as mongoose from 'mongoose';

export const ExerciseStepStatusSchema = new mongoose.Schema({
    exerciseStepId: {type: String, required: true},
    userId: {type: Number, required: true},
    status: {type: String, required: true},
    exerciseId: {type: Number, required: true},
});

export class ExerciseStepStatusModel{
    @IsNumber()
    @IsNotEmpty()    
    exerciseId: number;

    @IsString()
    @IsNotEmpty()    
    exerciseStepId: string;

    @IsNumber()
    @IsNotEmpty()    
    userId: number;

    @IsString()
    @IsNotEmpty()    
    status: string;
}
