import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors(); // dont't use this on production
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
