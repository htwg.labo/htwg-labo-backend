import { Body, Controller, Delete, Get, Param, Post, Put, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { DeletedModel } from 'src/models/deleted.model';
import { ExerciseStepStatusModel } from 'src/models/exercise-step-status';
import { ExerciseWithStepsModel } from 'src/models/exercise-with-steps.model';
import { ExerciseModel } from 'src/models/exercise.model';
import { ExerciseManagementService } from '../service/exercise-management.service';

@Controller('exercise-management')
export class ExerciseManagementController {

    constructor(private _exerciseManagementService: ExerciseManagementService){}

    /**
     * Upsert an exercise step status to mongodb.
    */
     @Post('/upsertExerciseStepStatus')
     @UsePipes(new ValidationPipe())
     async insertExerciseStepStatus(@Body() exerciseStepStatus: ExerciseStepStatusModel): Promise<any> {
         return this._exerciseManagementService.upsertExerciseStepStatus(exerciseStepStatus);
     }
 
     /**
      * Get an exercise with steps from mongodb.
     */
      @Get('/getExerciseStepStatus')
      @UsePipes(new ValidationPipe())
      async getExerciseStepStatus(@Query() query: {exerciseStepId: string, userId: number}): Promise<any> {
          return this._exerciseManagementService.getExerciseStepStatus(query.exerciseStepId, query.userId);
      }
      
    /**
     * Insert an exercise with steps to mongodb.
    */
    @Put('/insertExerciseWithSteps')
    @UsePipes(new ValidationPipe())
    async insertExerciseWithSteps(@Body() exerciseWithSteps: ExerciseWithStepsModel): Promise<ExerciseWithStepsModel> {
        return this._exerciseManagementService.insertExerciseWithSteps(exerciseWithSteps);
    }

    /**
     * Get an exercise with steps from mongodb.
    */
     @Get('/getExerciseWithSteps/:exerciseId')
     @UsePipes(new ValidationPipe())
     async getExerciseWithSteps(@Param('exerciseId') exerciseId: number): Promise<ExerciseWithStepsModel> {
         return this._exerciseManagementService.getExerciseWithSteps(exerciseId);
     }

    /**
     * Delete an exercise with steps in mongodb.
    */
     @Delete('/deleteExerciseWithSteps/:exerciseId')
     @UsePipes(new ValidationPipe())
     async deleteExerciseWithSteps(@Param('exerciseId') exerciseId: number): Promise<DeletedModel> {
         return this._exerciseManagementService.deleteExerciseWithSteps(exerciseId);
     }

    /**
     * Gets the exercises of a student from moodle.
    */
    @Get('/getStudentExercises')
    @UsePipes(new ValidationPipe())
    async studentExercises(@Query('token') token: string): Promise<ExerciseModel[]> {
        return this._exerciseManagementService.getStudentExercisesFromMoodle(token);
    }

    /**
     * Gets the exercises of a lecturer from moodle.
    */
     @Get('/getLecturerExercises')
     @UsePipes(new ValidationPipe())
     async lecturerExercises(@Query('token') token: string): Promise<ExerciseModel[]> {
         return this._exerciseManagementService.getLecturerExercisesFromMoodle(token);
     }

    /**
     * Get an exercise of a lecturer from moodle.
    */
     @Get('/getLecturerExercise')
     @UsePipes(new ValidationPipe())
     async lecturerExercise(@Query() query: {token: string, exerciseId: number}): Promise<ExerciseModel> {
         return this._exerciseManagementService.getLectuterExerciseFromMoodle(query.exerciseId, query.token);
     }

    /**
     * Get an exercise of a student from moodle.
    */
     @Get('/getStudentExercise')
     @UsePipes(new ValidationPipe())
     async studentExercise(@Query() query: {token: string, exerciseId: number}): Promise<ExerciseModel> {
         return this._exerciseManagementService.getStudentExerciseFromMoodle(query.exerciseId, query.token);
     }
}