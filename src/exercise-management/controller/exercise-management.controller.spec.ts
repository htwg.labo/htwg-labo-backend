import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseManagementController } from './exercise-management.controller';

describe('ExerciseManagementController', () => {
  let controller: ExerciseManagementController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExerciseManagementController],
    }).compile();

    controller = module.get<ExerciseManagementController>(ExerciseManagementController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
