import { HttpService } from '@nestjs/axios';
import { HttpException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, combineLatest, forkJoin, from, lastValueFrom, map, of, switchMap, tap, zip } from 'rxjs';
import { ExerciseModel } from 'src/models/exercise.model';
import { TokenModel } from 'src/models/token.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ExerciseWithStepsModel } from 'src/models/exercise-with-steps.model';
import { ExerciseStepStatusModel } from 'src/models/exercise-step-status';
import { UserManagementService } from 'src/user-management/service/user-management.service';

@Injectable()
export class ExerciseManagementService {
    private readonly _moodleBaseUrl = this._configService.get<string>('moodleBaseUrl');

    constructor(
        @InjectModel('ExerciseWithSteps') private readonly _exerciseWithStepsModel: Model<ExerciseWithStepsModel>,
        @InjectModel('ExerciseStepStatus') private readonly _exerciseStepStatusModel: Model<ExerciseStepStatusModel>,
        private readonly _httpService: HttpService, 
        private readonly _configService: ConfigService,
        private readonly _userManagementService: UserManagementService
    ){}

    /**
     * Upsert status of an exercise steps to mongodb.
    */
    public async upsertExerciseStepStatus(exerciseStepStatus: ExerciseStepStatusModel){
        const newExerciseStepStatus = {
            exerciseId: exerciseStepStatus.exerciseId,
            exerciseStepId: exerciseStepStatus.exerciseStepId,
            userId: exerciseStepStatus.userId,
            status: exerciseStepStatus.status,
        };
        await this._exerciseStepStatusModel.findOneAndUpdate({exerciseId: exerciseStepStatus.exerciseId, exerciseStepId: exerciseStepStatus.exerciseStepId, userId: exerciseStepStatus.userId},newExerciseStepStatus,{upsert: true});
        const result = await this._exerciseStepStatusModel.findOne({exerciseId: exerciseStepStatus.exerciseId, exerciseStepId: exerciseStepStatus.exerciseStepId, userId: exerciseStepStatus.userId});
        return result;
    }

    /**
     * Get an exercise step status from mongodb.
    */
     public async getExerciseStepStatus(exerciseStepId: string, userId: number){
        const exerciseStepStatus = await this._exerciseStepStatusModel.findOne({exerciseStepId: exerciseStepId, userId: userId});
        return exerciseStepStatus;
    }

    /**
     * Insert an exercise with steps to mongodb.
    */
    public async insertExerciseWithSteps(exerciseWithSteps: ExerciseWithStepsModel){
        // Check if exercise has already defined steps:
        const possibleDouble = await this.getExerciseWithSteps(exerciseWithSteps.exerciseId);
        if(possibleDouble){
            throw new HttpException('This exercise has already defined exercise steps.', 409);
        }

        // Check if the exercise steps have different orders:
        let orders: number[] = [];
        exerciseWithSteps.steps.forEach((step)=> {
            orders.push(step.order);
        });
        const hasDuplicateOrders = (new Set(orders)).size !== orders.length

        if(hasDuplicateOrders){
            throw new HttpException('The exercise steps have duplicate orders.', 409);
        }

        const newExerciseWithSteps = new this._exerciseWithStepsModel({
            exerciseId: exerciseWithSteps.exerciseId,
            steps: exerciseWithSteps.steps
        });
        const result = await newExerciseWithSteps.save();
        return result;
    }

    /**
     * Delete an exercise with steps in mongodb.
    */
     public async deleteExerciseWithSteps(exerciseId: number){
        const result = await this._exerciseWithStepsModel.deleteOne({exerciseId: exerciseId});
        await this._exerciseStepStatusModel.deleteMany({exerciseId:exerciseId});
        return result;
     }

    /**
     * Get an exercise with steps from mongodb.
    */
    public async getExerciseWithSteps(exerciseId: number){
        const exerciseWithSteps = await this._exerciseWithStepsModel.findOne({exerciseId: exerciseId});
        return exerciseWithSteps;
    }

    /**
     * Gets the exercises of a student from moodle.
    */
    public async getStudentExercisesFromMoodle(token: string): Promise<ExerciseModel[]> {
        let exercises: ExerciseModel[];
        const httpRequest = this._httpService.get(this._moodleBaseUrl + '/webservice/rest/server.php?wstoken=' + token + '&wsfunction=mod_assign_get_assignments&moodlewsrestformat=json');
        const exercisesResponse = await lastValueFrom(
            combineLatest([httpRequest, this.getLecturerExercisesFromMoodle(token)])
            .pipe(
            catchError(e => {
                throw new HttpException(e.response.data, e.response.status);
            }),
            map( ([studentExercises, lecturerExercises]) => {
                if(studentExercises.data.hasOwnProperty('courses') && studentExercises.data !== undefined){
                    let exe: ExerciseModel[] = [];
                    studentExercises.data.courses.forEach((course: any)=>{
                        course.assignments.forEach((assignment: any)=>{
                            if(!lecturerExercises.some((lecturerExercise)=> lecturerExercise.id === assignment.id)){
                                exe.push({
                                    courseid: course.id,
                                    coursefullname: course.fullname,
                                    courseshortname: course.shortname,
                                    id: assignment.id,
                                    name: assignment.name,
                                    duedate: assignment.duedate,
                                    allowsubmissionsfromdate: null,
                                    intro: assignment.intro,
                                    hasDefinedSteps: false
                                });                                
                            }
                        });
                    });
                    return exe;
                } else return [];
            }
            ),
            switchMap((exercises:ExerciseModel[]) => {
                if(exercises.length > 0){
                    const newExercises = exercises.map(exercise => from(this.getExerciseWithSteps(exercise.id)).pipe(
                        catchError(e => {
                            throw new HttpException(e.response.data, e.response.status);
                        }),
                        map((res) => {
                            return {
                                ...exercise,
                                hasDefinedSteps: res !== null
                            }
                        }),
                    ));
                    return forkJoin(newExercises);                    
                }
                else return of([]);
            }),
            tap((exercises:ExerciseModel[])=> {
                exercises = exercises;
            })
        ));
        if(exercises){
            return exercises;
        }
        return exercisesResponse
    }

    /**
     * Gets the exercises of a lecturer from moodle.
    */
     public async getLecturerExercisesFromMoodle(token: string): Promise<ExerciseModel[]> {
        let exercises: ExerciseModel[];
        //const httpRequest = this._httpService.get(this._moodleBaseUrl + '/webservice/rest/server.php?wstoken=' + token + '&wsfunction=mod_assign_get_assignments&moodlewsrestformat=json');
        const exercisesResponse = await lastValueFrom(
            from(this._userManagementService.getUserDataFromMoodle(token))  
            .pipe(
            switchMap((user)=>
                zip(of(user), this._httpService.get(this._moodleBaseUrl + '/webservice/rest/server.php?wstoken=' + token + '&wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json&userid=' + user.userid))
            ),
            switchMap(([user, coursesResp])=> {
                if(!coursesResp || !user){
                    return of([]);
                }
                if(Array.isArray(coursesResp.data)){
                    const allVisibleCourses: number[] = coursesResp.data.filter((course)=> course.visible === 1).map((course)=>course.id);
                    let queryString: string = '';
                    allVisibleCourses.forEach((courseId, index) => {
                        queryString = queryString + '&courseids[' + index + ']=' + courseId
                    })
                    if(queryString == ''){
                        return zip(of(null), of(user));
                    }
                    return zip(this._httpService.get(this._moodleBaseUrl + '/webservice/rest/server.php?wstoken=' + token + '&wsfunction=mod_assign_get_assignments&moodlewsrestformat=json'+ queryString), of(user))
                }       
                else return zip(of(null), of(user));         
            }
            ),
            switchMap(([coursesResp, user]) => {
                if(!coursesResp || !user){
                    return of([]);
                }
                if(Array.isArray(coursesResp.data.courses)){
                    if(coursesResp.data.courses.length > 0){
                        const coursesEnrolledUsers = coursesResp.data.courses.map(course => this._httpService.get(this._moodleBaseUrl + '/webservice/rest/server.php?wstoken=' + token + `&wsfunction=core_enrol_get_enrolled_users&moodlewsrestformat=json&courseid=${course.id}`).pipe(
                            catchError(e => {
                                throw new HttpException(e.response.data, e.response.status);
                            }),
                            map((res) => {
                                const courseEnrolledUsers = res.data;
                                let lecturerCourse = {};
                                if(Array.isArray(courseEnrolledUsers)){
                                    courseEnrolledUsers.forEach((u:any)=> {
                                        if(u.id == user.userid){
                                            u.roles.forEach((role:any)=>{
                                                if(role.shortname === 'editingteacher'){
                                                    lecturerCourse = course;
                                                }
                                            })
                                        }
                                    })                                    
                                }
                                return lecturerCourse
                            }),
                        ));
                        return forkJoin(coursesEnrolledUsers);                    
                    } else return of([]);
                }
                else return of([]);
            }),
            map(courses => {
                    let exe: ExerciseModel[] = [];
                    if(Array.isArray(courses)){
                        courses.forEach((course: any)=>{
                            if(course.hasOwnProperty('assignments')){
                                course.assignments.forEach((assignment: any)=>{
                                    exe.push({
                                        courseid: course.id,
                                        coursefullname: course.fullname,
                                        courseshortname: course.shortname,
                                        id: assignment.id,
                                        name: assignment.name,
                                        duedate: assignment.duedate,
                                        allowsubmissionsfromdate: null,
                                        intro: assignment.intro,
                                        hasDefinedSteps: false
                                    });
                                });                                
                            }
                        });
                    }
                    return exe;
            }
            ),
            switchMap((exercises:ExerciseModel[]) => {
                if(exercises.length > 0){
                    const newExercises = exercises.map(exercise => from(this.getExerciseWithSteps(exercise.id)).pipe(
                        catchError(e => {
                            throw new HttpException(e.response.data, e.response.status);
                        }),
                        map((res) => {
                            return {
                                ...exercise,
                                hasDefinedSteps: res !== null
                            }
                        }),
                    ));
                    return forkJoin(newExercises);                    
                }
                else return of([]);
            }),
            tap((exercises:ExerciseModel[])=> {
                exercises = exercises;
            })
        ));
        if(exercises){
            return exercises;
        }
        return exercisesResponse
    }

    /**
     * Gets an exercercise of a lecturer from moodle.
    */
     public async getLectuterExerciseFromMoodle(id: number, token: string): Promise<ExerciseModel> {
        let exercise: ExerciseModel;
        const result = await lastValueFrom(from(this.getLecturerExercisesFromMoodle(token)).pipe(
            catchError(e => {
                throw new HttpException(e.response.data, e.response.status);
            }),
            map((exercises:ExerciseModel[]) => {
                exercise = exercises.find((exercise)=> 
                    exercise.id == id
                );
                return exercise;
            })
        ));
        if(exercise){
            return exercise;
        }
        return result;
     }

    /**
     * Gets an exercercise of a student from moodle.
    */
     public async getStudentExerciseFromMoodle(id: number, token: string): Promise<ExerciseModel> {
        let exercise: ExerciseModel;
        const result = await lastValueFrom(from(this.getStudentExercisesFromMoodle(token)).pipe(
            catchError(e => {
                throw new HttpException(e.response.data, e.response.status);
            }),
            map((exercises:ExerciseModel[]) => {
                exercise = exercises.find((exercise)=> 
                    exercise.id == id
                );
                return exercise;
            })
        ));
        if(exercise){
            return exercise;
        }
        return result;
     }
}
