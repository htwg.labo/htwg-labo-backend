import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseManagementService } from './exercise-management.service';

describe('ExerciseManagementService', () => {
  let service: ExerciseManagementService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExerciseManagementService],
    }).compile();

    service = module.get<ExerciseManagementService>(ExerciseManagementService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
