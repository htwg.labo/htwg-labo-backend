import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ExerciseStepStatusSchema } from 'src/models/exercise-step-status';
import { ExerciseWithStepsSchema } from 'src/models/exercise-with-steps.model';
import { UserManagementModule } from 'src/user-management/user-management.module';
import { ExerciseManagementController } from './controller/exercise-management.controller';
import { ExerciseManagementService } from './service/exercise-management.service';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'ExerciseWithSteps', schema: ExerciseWithStepsSchema},{name: 'ExerciseStepStatus', schema: ExerciseStepStatusSchema}]),
    HttpModule,
    ConfigModule,
    UserManagementModule
  ],
  controllers: [ExerciseManagementController],
  providers: [ExerciseManagementService]
})
export class ExerciseManagementModule {}
