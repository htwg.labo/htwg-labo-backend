import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
     async welcome(): Promise<string> {
        return "Welcome to Labo!"
    }
}
