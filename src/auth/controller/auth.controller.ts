import { Body, Controller, Get, Post, Req, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TokenModel } from '../../models/token.model';
import { AuthUserModel } from '../../models/auth-user.model';
import { AuthService } from '../service/auth.service';

@Controller('auth')
export class AuthController {

    constructor(private _authService: AuthService){}

    /**
     * Gets the access token from moodle login.
    */
    @UseGuards(AuthGuard('local'))
    @Post('/token')
    @UsePipes(new ValidationPipe())
    async token(@Body() user: AuthUserModel): Promise<TokenModel> {
        return this._authService.getTokenFromMoodleLogin(user);
    }
}
