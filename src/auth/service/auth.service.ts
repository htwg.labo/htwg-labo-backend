import { HttpService } from '@nestjs/axios';
import { HttpException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom, map, tap } from 'rxjs';
import { TokenModel } from '../../models/token.model';
import { AuthUserModel } from '../../models/auth-user.model';

@Injectable()
export class AuthService {

    private readonly _moodleBaseUrl = this._configServic.get<string>('moodleBaseUrl');

    constructor(
        private readonly _httpService: HttpService, 
        private readonly _configServic: ConfigService,
    ){}

    /**
     * Gets the access token from moodle login.
    */
    async getTokenFromMoodleLogin(user: AuthUserModel): Promise<TokenModel> {
        let token: TokenModel;
        const httpRequest = this._httpService.get(this._moodleBaseUrl + '/login/token.php?username=' + user.username + '&password=' + user.password + '&service=moodle_mobile_app');
        const tokenResponse = await lastValueFrom(httpRequest.pipe(
            catchError(e => {
                throw new HttpException(e.response.data, e.response.status);
            }),
            map(resp => resp.data),
            tap(data => {
                if(data.hasOwnProperty('token')){
                    token = {
                        token: data.token
                    };                    
                }
            }),
        ));
        if(token){
            return token;
        }
        return tokenResponse
    }
}
